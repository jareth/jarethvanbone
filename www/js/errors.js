(function() {
  var NotFound, config, setup,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  config = require("./config.coffee");

  NotFound = (function(superClass) {
    extend(NotFound, superClass);

    function NotFound(name) {
      this.name = name;
      NotFound.__super__.constructor.call(this, this.name);
    }

    return NotFound;

  })(Error);

  setup = function(app) {
    if (!config.errorPages) {
      return;
    }
    return app.get(/error(\d+)/, function(req, res) {
      var code;
      code = req.params[0];
      switch (code) {
        case "500":
        case "502":
        case "503":
        case "504":
          return res.render("errors/error500");
        default:
          return res.render("errors/error404");
      }
    });
  };

  module.exports = {
    NotFound: NotFound,
    setup: setup
  };

}).call(this);
