(function() {
  var AUTOUSING_THE_ROUTER_IS_A_NUISANCE, app, config, errors, express, i, len, name, ref, server;

  config = require('./config.coffee');

  errors = require('./errors.coffee');

  express = require('express');

  app = express();

  app.set('views', __dirname + '/templates');

  app.set('view engine', 'jade');

  app.use(express.logger({
    format: ':date :method :url'
  }));

  AUTOUSING_THE_ROUTER_IS_A_NUISANCE = app.router;

  ref = ['./site/routes.coffee'];
  for (i = 0, len = ref.length; i < len; i++) {
    name = ref[i];
    require(name)(app);
  }

  app.use(app.router);

  app.use(express["static"](config.staticDir));

  errors.setup(app);

  app.use(function(req, res, next) {
    return next(new errors.NotFound(req.path));
  });

  app.use(function(error, req, res, next) {
    console.log("Error handler middleware:", error);
    if (error instanceof errors.NotFound) {
      return res.render("errors/error404");
    } else {
      return res.render("errors/error500");
    }
  });

  server = app.listen(process.env.PORT);

  console.log('Express server listening on %s:%d', process.env.IP, process.env.PORT);

}).call(this);
