(function() {
  this.Train = (function() {
    var distant1, distant2, fore1, fore2, game, middle1, middle2, speed, train, tweenComplete;

    game = null;

    train = null;

    distant1 = null;

    distant2 = null;

    middle1 = null;

    middle2 = null;

    fore1 = null;

    fore2 = null;

    speed = 10;

    function Train(tmpGame) {
      game = tmpGame;
    }

    Train.prototype.preload = function() {
      game.load.image('sky', 'images/sky.png');
      game.load.image('distant', 'images/distant.png');
      game.load.image('middle', 'images/middle.png');
      game.load.image('fore', 'images/fore.png');
      game.load.image('train', 'images/window.png');
    };

    Train.prototype.create = function() {
      var ratio, trainTween;
      game.add.sprite(0, 0, 'sky');
      distant1 = game.add.sprite(0, 0, 'distant');
      distant1.y = game.height - distant1.height;
      distant2 = game.add.sprite(0, distant1.y, 'distant');
      distant2.x = game.width + 50;
      middle1 = game.add.sprite(0, 0, 'middle');
      middle1.y = game.height - middle1.height;
      middle2 = game.add.sprite(0, middle1.y, 'middle');
      middle2.x = game.width + 50;
      fore1 = game.add.sprite(0, 0, 'fore');
      fore1.y = game.height - fore1.height;
      fore2 = game.add.sprite(0, fore1.y, 'fore');
      fore2.x = game.width + 50;
      train = game.add.sprite(0, 0, 'train');
      ratio = (game.world.width + 50) / train.width;
      train.scale.x = ratio;
      train.scale.y = ratio;
      train.x = train.baseX = (game.world.width - train.width) / 2;
      train.y = train.baseY = (game.world.height - train.height) / 2;
      trainTween = game.tweens.create(train);
      trainTween.onComplete.add(tweenComplete, this);
      trainTween.to({
        x: train.baseX + (Math.random() * 10),
        y: train.baseY + (Math.random() * 10)
      }, 1000, Phaser.Easing.Linear.None, true);
    };

    Train.prototype.update = function() {
      distant1.x = distant1.x < -distant1.width ? game.width : distant1.x - speed / 10;
      distant2.x = distant2.x < -distant2.width ? game.width : distant2.x - speed / 10;
      middle1.x = middle1.x < -middle1.width ? game.width : middle1.x - speed / 5;
      middle2.x = middle2.x < -middle2.width ? game.width : middle2.x - speed / 5;
      fore1.x = fore1.x < -fore1.width ? game.width : fore1.x - speed;
      fore2.x = fore2.x < -fore2.width ? game.width : fore2.x - speed;
    };

    tweenComplete = function() {
      var trainTween;
      trainTween = game.tweens.create(train);
      trainTween.onComplete.add(tweenComplete, this);
      trainTween.to({
        x: train.baseX + (Math.random() * 10),
        y: train.baseY + (Math.random() * 10)
      }, 1000, Phaser.Easing.Linear.None, true);
    };

    return Train;

  })();

}).call(this);
