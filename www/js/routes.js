(function() {
  var attg, bayou, danceacademy, danceacademy2, dharmawantsyou, express, find815, home, portefeuillo, projects, slide, timelapse, timeline;

  express = require('express');

  home = function(req, res) {
    return res.render('index', {
      title: 'Jareth van Bone'
    });
  };

  projects = function(req, res) {
    return res.render('projects', {
      title: 'Projects'
    });
  };

  portefeuillo = function(req, res) {
    return res.render('portefeuillo', {
      title: 'Portefeuillo'
    });
  };

  bayou = function(req, res) {
    return res.render('bayou', {
      title: 'Bayou Blast'
    });
  };

  timelapse = function(req, res) {
    return res.render('timelapse', {
      title: 'Timelapse Animations'
    });
  };

  timeline = function(req, res) {
    return res.render('timeline', {
      title: 'Timeline'
    });
  };

  danceacademy = function(req, res) {
    return res.render('danceacademy', {
      title: 'Dance Academy'
    });
  };

  danceacademy2 = function(req, res) {
    return res.render('danceacademy2', {
      title: 'Dance Academy 2nd Year'
    });
  };

  find815 = function(req, res) {
    return res.render('find815', {
      title: 'Lost - Find815'
    });
  };

  dharmawantsyou = function(req, res) {
    return res.render('dharmawantsyou', {
      title: 'Lost - Dharma Wants You'
    });
  };

  attg = function(req, res) {
    return res.render('attg', {
      title: 'Australia: The Time Traveller\'s Guide'
    });
  };

  slide = function(req, res) {
    return res.render('slide', {
      title: 'SLiDE'
    });
  };

  module.exports = function(app) {
    app.get('/', home);
    app.get('/projects', projects);
    app.get('/bayou', bayou);
    app.get('/timelapse', timelapse);
    app.get('/portefeuillo', portefeuillo);
    app.get('/timeline', timeline);
    app.get('/danceacademy', danceacademy);
    app.get('/danceacademy2', danceacademy2);
    app.get('/find815', find815);
    app.get('/dharmawantsyou', dharmawantsyou);
    app.get('/attg', attg);
    return app.get('/slide', slide);
  };

}).call(this);
