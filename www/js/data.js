var DIALOGUES = {
    girl: {
        "actors": [
            {
                "id": 10,
                "name": "Jareth"
            },
            {
                "id": 20,
                "name": "Visitor"
            }
        ],
        "dialogues": [
            {
                "id": 10,
                "parent": null,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "I'm glad you made it, I was beginning to think you wouldn't come.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    20
                ]
            },
            {
                "id": 20,
                "parent": 10,
                "isChoice": false,
                "actor": 20,
                "conversant": 10,
                "menuText": "",
                "dialogueText": "Who are you?",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    30
                ]
            },
            {
                "id": 30,
                "parent": 20,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "Oh, sorry. Hi, my name is Jareth van Bone",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    40
                ]
            },
            {
                "id": 40,
                "parent": 30,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "Come along, we've got a long way to go and we can discuss everything along the way",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    45
                ]
            },
            {
                "id": 45,
                "parent": 40,
                "isChoice": true,
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    50,
                    60
                ]
            },
            {
                "id": 50,
                "parent": 45,
                "isChoice": false,
                "actor": 20,
                "conversant": 10,
                "menuText": "What is the place?",
                "dialogueText": "Where am I?",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    70
                ]
            },
            {
                "id": 60,
                "parent": 45,
                "isChoice": false,
                "actor": 20,
                "conversant": 10,
                "menuText": "There must be some mistake...",
                "dialogueText": "I was looking for someone's portfolio.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    130
                ]
            },
            {
                "id": 70,
                "parent": 50,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "This is Portefeuillo Station, I created it.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    80
                ]
            },
            {
                "id": 80,
                "parent": 70,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "This station, the city outside, the whole world, even you ... well the body you have anyway.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    90
                ]
            },
            {
                "id": 90,
                "parent": 80,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "I sat down to build my portfolio website one night and realised I didn't really want to build a portfolio website.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    100
                ]
            },
            {
                "id": 100,
                "parent": 90,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "I wanted to build a world instead, a story, where someone could get to know me.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    110
                ]
            },
            {
                "id": 110,
                "parent": 100,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "I thought: if I've spent so many years building interactive story experiences for the entertainment industry...",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    120
                ]
            },
            {
                "id": 120,
                "parent": 110,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "What's to stop me building one for myself?",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": []
            },
            {
                "id": 130,
                "parent": 60,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "In a hurry huh? That's understandable, seems like the entire world is in some kind whirlwind frenzy these days.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    140
                ]
            },
            {
                "id": 140,
                "parent": 130,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "I keep a bunch of professional information over at LinkedIn (http://au.linkedin.com/in/jareth/)",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    150
                ]
            },
            {
                "id": 150,
                "parent": 140,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "or you can browse the code for this site at BitBucket (https://bitbucket.org/jareth/jarethvanbone)",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    160
                ]
            },
            {
                "id": 160,
                "parent": 150,
                "isChoice": false,
                "actor": 10,
                "conversant": 20,
                "menuText": "",
                "dialogueText": "I hope you can make it back when you've got some more time to explore.",
                "conditionsString": "",
                "codeBefore": "",
                "codeAfter": "",
                "outgoingLinks": [
                    null
                ]
            }
        ]
    }
};