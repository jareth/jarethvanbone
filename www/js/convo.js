(function() {
  this.Convo = (function() {
    var game;

    game = null;

    function Convo(tmpGame) {
      game = tmpGame;
    }

    Convo.prototype.preload = function() {};

    Convo.prototype.create = function() {};

    Convo.prototype.update = function() {};

    return Convo;

  })();

}).call(this);
