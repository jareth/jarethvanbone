(function() {
  this.Game = (function() {
    var currentScene, game, onDialogReset, onDialogShow, train;

    game = null;

    train = null;

    currentScene = null;

    function Game(Phaser) {
      game = new Phaser.Game(970, 485, Phaser.AUTO, 'gameViewport', {
        preload: this.preload,
        create: this.create,
        update: this.update
      });
      console.log('new game ');
    }

    Game.prototype.preload = function() {
      train = new Train(game);
      train.preload();
    };

    Game.prototype.create = function() {
      var dialog;
      game.stage.scale.maxWidth = game.width;
      game.stage.scale.maxHeight = game.height;
      game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL;
      game.stage.scale.setShowAll();
      game.stage.scale.refresh();
      currentScene = train;
      currentScene.create();
      dialog = new Dialog(DIALOGUES.girl, onDialogReset, onDialogShow);
      dialog.show();
    };

    Game.prototype.update = function() {
      train.update();
    };

    onDialogReset = function() {};

    onDialogShow = function(event) {};

    return Game;

  })();

}).call(this);
