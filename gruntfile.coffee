module.exports = (grunt)->

    grunt.initConfig
        pkg: grunt.file.readJSON('package.json')

        coffee:
            product:
                expand: true
                flatten: true
                cwd: 'app'
                src: ['**/*.coffee']
                dest: 'www/js/'
                ext: '.js'

        sass:
            options:
                includePaths: ['www/bower_components/foundation/scss']
            dist:
                #options:
                    #outputStyle: 'compressed'
                files:
                    [
                        expand: true
                        cwd: 'app'
                        src: ['**/*.scss', '**/*.sass']
                        dest: 'www/css/'
                        ext: '.css'
                    ]

        express:
            coffee:
                options:
                    script: 'coffeeBootstrap.js'
                    #cmd: 'coffee'
                    port: process.env.PORT

        watch:
            grunt: 'gruntfile.coffee'

            sass:
                files: ['app/**/*.scss']
                tasks: ['sass']

            express:
                files: ['app/**/*.coffee']
                tasks: ['coffee', 'express:coffee']
                options:
                    spawn: false


    grunt.loadNpmTasks 'grunt-contrib-coffee'
    grunt.loadNpmTasks 'grunt-sass'
    grunt.loadNpmTasks 'grunt-contrib-watch'
    grunt.loadNpmTasks 'grunt-express-server'

    grunt.registerTask 'dev', ['build', 'express:coffee', 'watch']
    grunt.registerTask 'build', ['coffee', 'sass']
    grunt.registerTask 'default', ['dev']
