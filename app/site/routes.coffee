express = require 'express'

home = (req, res)->
    res.render 'index',
        title: 'Jareth van Bone'

projects = (req, res)->
    res.render 'projects',
        title: 'Projects'

portefeuillo = (req, res)->
    res.render 'portefeuillo',
        title: 'Portefeuillo'

bayou = (req, res)->
    res.render 'bayou',
        title: 'Bayou Blast'

timelapse = (req, res)->
    res.render 'timelapse',
        title: 'Timelapse Animations'

timeline = (req, res)->
    res.render 'timeline',
        title: 'Timeline'

danceacademy = (req, res)->
    res.render 'danceacademy',
        title: 'Dance Academy'

danceacademy2 = (req, res)->
    res.render 'danceacademy2',
        title: 'Dance Academy 2nd Year'

find815 = (req, res)->
    res.render 'find815',
        title: 'Lost - Find815'

dharmawantsyou = (req, res)->
    res.render 'dharmawantsyou',
        title: 'Lost - Dharma Wants You'

attg = (req, res)->
    res.render 'attg',
        title: 'Australia: The Time Traveller\'s Guide'

slide = (req, res)->
    res.render 'slide',
        title: 'SLiDE'

module.exports = (app) ->
    app.get '/', home
    app.get '/projects', projects
    app.get '/bayou', bayou
    app.get '/timelapse', timelapse
    app.get '/portefeuillo', portefeuillo
    app.get '/timeline', timeline
    app.get '/danceacademy', danceacademy
    app.get '/danceacademy2', danceacademy2
    app.get '/find815', find815
    app.get '/dharmawantsyou', dharmawantsyou
    app.get '/attg', attg
    app.get '/slide', slide