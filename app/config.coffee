projectRoot = "#{__dirname}/.."
exports.appURI = "/app"
exports.errorPages = true

exports.staticDir = "#{projectRoot}/www"
exports.tests = false