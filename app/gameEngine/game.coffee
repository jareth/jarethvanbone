class @Game

    game = null
    train = null
    currentScene = null

    constructor: (Phaser) ->
        game = new Phaser.Game 970, 485, Phaser.AUTO, 'gameViewport',
            preload: this.preload
            create: this.create
            update: this.update
        console.log 'new game '

    preload: () ->

        train = new Train game
        train.preload()

        return

    create: () ->

        game.stage.scale.maxWidth = game.width
        game.stage.scale.maxHeight = game.height
        game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL
        game.stage.scale.setShowAll()
        game.stage.scale.refresh()

        currentScene = train
        currentScene.create()

        dialog = new Dialog DIALOGUES.girl, onDialogReset, onDialogShow
        dialog.show()

        return

    update: () ->

        train.update()

        return

    onDialogReset = () ->

        return

    onDialogShow = ( event ) ->

        return