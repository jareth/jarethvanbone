class @Train

    game = null
    train = null
    distant1 = null
    distant2 = null
    middle1 = null
    middle2 = null
    fore1 = null
    fore2 = null
    speed = 10

    constructor: (tmpGame) ->
        game = tmpGame

    preload: () ->

        game.load.image 'sky', 'images/sky.png'
        game.load.image 'distant', 'images/distant.png'
        game.load.image 'middle', 'images/middle.png'
        game.load.image 'fore', 'images/fore.png'
        game.load.image 'train', 'images/window.png'

        return

    create: () ->

        # sky
        game.add.sprite 0, 0, 'sky'

        # distant
        distant1 = game.add.sprite 0, 0, 'distant'
        distant1.y = game.height - distant1.height
        distant2 = game.add.sprite 0, distant1.y, 'distant'
        distant2.x = game.width + 50

        # middle
        middle1 = game.add.sprite 0, 0, 'middle'
        middle1.y = game.height - middle1.height
        middle2 = game.add.sprite 0, middle1.y, 'middle'
        middle2.x = game.width + 50

        # fore
        fore1 = game.add.sprite 0, 0, 'fore'
        fore1.y = game.height - fore1.height
        fore2 = game.add.sprite 0, fore1.y, 'fore'
        fore2.x = game.width + 50

        # train
        train = game.add.sprite 0, 0, 'train'
        ratio = (game.world.width + 50) / train.width
        train.scale.x = ratio
        train.scale.y = ratio
        train.x = train.baseX = (game.world.width - train.width) / 2
        train.y = train.baseY = (game.world.height - train.height) / 2

        trainTween = game.tweens.create train
        trainTween.onComplete.add tweenComplete, this
        trainTween.to
            x: train.baseX + (Math.random() * 10)
            y: train.baseY + (Math.random() * 10)
        , 1000, Phaser.Easing.Linear.None, true

        return

    update: () ->

        distant1.x = if distant1.x < -distant1.width then game.width else distant1.x - speed / 10
        distant2.x = if distant2.x < -distant2.width then game.width else distant2.x - speed / 10

        middle1.x = if middle1.x < -middle1.width then game.width else middle1.x - speed / 5
        middle2.x = if middle2.x < -middle2.width then game.width else middle2.x - speed / 5

        fore1.x = if fore1.x < -fore1.width then game.width else fore1.x - speed
        fore2.x = if fore2.x < -fore2.width then game.width else fore2.x - speed

        return

    tweenComplete = () ->

        trainTween = game.tweens.create train
        trainTween.onComplete.add tweenComplete, this
        trainTween.to
            x: train.baseX + (Math.random() * 10)
            y: train.baseY + (Math.random() * 10)
        , 1000, Phaser.Easing.Linear.None, true

        return