#!/usr/bin/env coffee
# if process.env.NODETIME_ACCOUNT_KEY
#   require('nodetime').profile
#     accountKey: process.env.NODETIME_ACCOUNT_KEY
#     appName: 'jareth-heroku'
# else
#   require('nodetime').profile
#     accountKey: 'ab3f3bdf2fea75f268bcb1d6f92a2bb5fe48e9be'
#     appName: 'jareth-dev'
config = require './config.coffee'
errors = require './errors.coffee'
express = require 'express'

app = express()

# setup templating - using jade
app.set 'views', __dirname + '/templates'
app.set 'view engine', 'jade'

# setup logging - todo: investigate "winston"
app.use express.logger {format: ':date :method :url'}

# from https://github.com/focusaurus/express_code_structure/blob/master/app/server.js
AUTOUSING_THE_ROUTER_IS_A_NUISANCE = app.router;

# load routes (controllers)
require(name)(app) for name in [
    './site/routes.coffee'
]
# now we can use app.router
app.use app.router

app.use express.static config.staticDir

# setup error handling
errors.setup app

#Last in the chain means 404 for you
app.use (req, res, next) ->
    next new errors.NotFound req.path

# other errors
app.use (error, req, res, next) ->
  console.log "Error handler middleware:", error
  if error instanceof errors.NotFound
    res.render "errors/error404"
  else
    res.render "errors/error500"

# start server
server = app.listen process.env.PORT
console.log 'Express server listening on %s:%d', process.env.IP, process.env.PORT