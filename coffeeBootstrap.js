/**
 * restarting a coffee executable (like coffee app/app.coffee)
 * via either nodemon or grunt-express-server fails to kill the original
 * process, ending in a EADDRINUSE error.
 *
 * This bootstrap file is a workaround to use the correctly functioning
 * node executable.
 */

require('coffee-script/register')
require('./app/app.coffee')